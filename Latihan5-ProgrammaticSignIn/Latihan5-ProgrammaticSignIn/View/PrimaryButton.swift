//
//  PrimaryButton.swift
//  Latihan5-ProgrammaticSignIn
//
//  Created by MACBOOK PRO on 27/03/23.
//

import UIKit

class PrimaryButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder){
        super.init(coder: coder)
    }
    
    func setup() {
        self.tintColor = .neutral3
        self.backgroundColor = .brand1
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }
    
    convenience init() {
        self.init(type: .system)
        setup()
    }

}

//
//  ContentView.swift
//  Latihan7-Prototyping
//
//  Created by MACBOOK PRO on 29/03/23.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        TabView{
            ArticleListView()
                .tabItem {
                    Image(systemName: "square.grid.2x2")
                    Text("Browse")
                }
            Text("Watch")
                .font(.system(.largeTitle, design: .rounded))
                .tabItem {
                Image(systemName: "play.rectangle")
                Text("Watch")
            }
            
            Text("Search")
                .font(.system(.largeTitle, design: .rounded))
                .tabItem {
                Image(systemName: "rectangle.and.text.magnifyingglass")
                Text("Search")
            }
            
           Settings()
                .tabItem {
                Image(systemName: "gear")
                Text("Profile")
            }
        } //:Tabview
        .tint(.purple)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}

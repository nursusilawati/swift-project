//
//  Latihan7_PrototypingApp.swift
//  Latihan7-Prototyping
//
//  Created by MACBOOK PRO on 29/03/23.
//

import SwiftUI

@main
struct Latihan7_PrototypingApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}

//
//  Setting.swift
//  Latihan7-Prototyping
//
//  Created by MACBOOK PRO on 30/03/23.
//

import SwiftUI

struct Settings: View {
    var body: some View {
        ScrollView(.vertical, showsIndicators: false)
        {
            
            VStack(spacing: 20) {
                
                // MARK: - SECTION INFO
                sectionInfo
                
                //MARK: SECTION APPLICATION
                GroupBox {
                    Divider().padding(.vertical, 4)
                        
                    SettingsRow(name: "Developeer", content: "Nur Susilawati")
                    
                    SettingsRow(name: "Company", content: "BCA Syariah")
                        
                    SettingsRow(name: "Compability", content: "iOS 16")
                        
                    SettingsRow(name: "Website", linkLabel: "BCAS", linkDestination: "bcasyariah.co.id")
                    
                } label: {
                    SettingLabelView(labelText: "App", labelImage: "pin")
                }
                .padding()
            }// VSTACK
            
        }//: Scrollview
    }
}

struct Setting_Previews: PreviewProvider {
    static var previews: some View {
        Settings()
    }
}

//MARK: - SETTING VIEW
struct SettingLabelView: View {
    
    var labelText: String
    var labelImage: String
    
    var body: some View {
        HStack {
            Text(labelText.uppercased())
                .fontWeight(.bold)
            Spacer()
            Image(systemName: labelImage)
        }
    }
}

// MARK: - Extension
extension Settings {
    // 1. Setting Info
    private var sectionInfo: some View {
        GroupBox {
            Divider().padding(.vertical, 4)
            
            HStack (alignment: .center, spacing: 10)
            {
                Image("logo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 80, height: 80)
                    .cornerRadius(10)
                
                Text("SwiftUI helps you build great-looking apps across all Apple Platforms with the power of swift language, and surpriingly litle code. You can bring even better experiences to everyone, on any Apple device usong just one set of tools and APIs")
                    .font(.system(size: 12))
            }
            
        } label: {
            SettingLabelView(labelText: "App Info", labelImage: "info.circle")
        }
        .padding()
        
    }
}
//MARK: - SETTINGs ROW

struct SettingsRow: View {
    
    var name: String
    var content: String?
    var linkLabel: String?
    var linkDestination: String?
    
    var body: some View {
        HStack {
            Text(name)
                .foregroundColor(.gray)
            Spacer()
            if (content != nil) {
                Text(content ?? "N/A")
            } else if (linkLabel != nil && linkDestination != nil) {
                Link(linkLabel!, destination: URL(string: "https://\( linkDestination!)")!)
                
                Image(systemName: "arrow.up.right.square")
                    .foregroundColor(.purple)
            } else {
                EmptyView()
            }
        }.padding(4)
    }
}

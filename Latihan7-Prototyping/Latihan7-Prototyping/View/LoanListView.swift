//
//  LoanListView.swift
//  Latihan7-Prototyping
//
//  Created by MACBOOK PRO on 31/03/23.
//

import SwiftUI

struct LoanListView: View {
    
    @ObservedObject var loanStore = LoanStore()
    
    var body: some View {
        NavigationStack {
            List(loanStore.loans) { loan in
                LoanRowView(loan: loan)
                    .padding(.vertical, 6)
                
            }
            .listStyle(InsetListStyle())
            .task {
                self.loanStore.fetchLatestLoans()
            }
        }
    }
}

struct LoanListView_Previews: PreviewProvider {
    static var previews: some View {
        LoanListView()
    }
}

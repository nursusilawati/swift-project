//
//  LoanRowView.swift
//  Latihan7-Prototyping
//
//  Created by MACBOOK PRO on 31/03/23.
//

import SwiftUI

struct LoanRowView: View {
    
    var loan: Loan
    
    var body: some View {
        HStack {
            VStack (alignment: .leading) {
                Text(loan.name)
                    .font(.system(.headline, design: .rounded))
                    .fontWeight(.bold)
                
                Text(loan.country)
                    .font(.system(.subheadline, design: .rounded))
                
                Text(loan.use)
                    .font(.system(.body, design: .rounded))
            } // : VSTACK
            
            Spacer ()
            Text("$\(loan.amount)")
                .font(.system(.title, design: .rounded))
                .fontWeight(.bold)
        }//: HSTACK
        
        .padding()
        .frame(minWidth: 0, maxWidth: .infinity)
        
        
    }
}

struct LoanRowView_Previews: PreviewProvider {
    static var previews: some View {
        
        
        LoanRowView(loan: Loan(name: "Russel", country: "Indonesia", use: "For Buying MBP M1 Pro", amount: Int(4000)))
    }
}

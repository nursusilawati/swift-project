//
//  EmojiViewController.swift
//  Latihan3-StoryBoardSwift
//
//  Created by MACBOOK PRO on 24/03/23.
//

import UIKit


class EmojiViewController: UIViewController {

    @IBOutlet weak var emojiLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // stored property
    var emoji : EmojiModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        emojiLabel.text = emoji.emoji
        nameLabel.text = emoji.name
        descriptionLabel.text = emoji.description
    }

}
extension UIViewController {
    func showEmojiViewController(emoji: EmojiModel){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EmojiViewController") as! EmojiViewController
        viewController.emoji = emoji
        
        navigationController?.pushViewController(viewController, animated: true)
    }
}

//
//  EmojiViewCell.swift
//  Latihan3-StoryBoardSwift
//
//  Created by MACBOOK PRO on 24/03/23.
//

import UIKit

class EmojiViewCell: UITableViewCell {

    @IBOutlet weak var emojiLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

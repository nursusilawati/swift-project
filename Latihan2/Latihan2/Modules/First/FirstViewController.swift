//
//  FristViewController.swift
//  Latihan2
//
//  Created by MACBOOK PRO on 20/03/23.
//

import UIKit

class FirstViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    var onBoardingList: [onBoarding] = []
    
    var timer : Timer?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadOnBoardingList()
        collectionView.dataSource = self
        collectionView.delegate = self
        
        timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: {
            [weak self] _ in
            guard let `self` = self else {return}
            
            let currentPage = Int(self.collectionView.contentOffset.x / self.collectionView.frame.width)
            let nextPage = currentPage + 1 >= self.onBoardingList.count ? 0 : currentPage + 1
            let indexPath = IndexPath(item: nextPage, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.pageControl.currentPage = nextPage
        })
    }
    
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        showPinViewController()
    }
    
    // MARK: - UICollectionDataSources
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return onBoardingList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "onBoarding_cell", for: indexPath) as! OnBoardingViewCell
        
        let onBoarding = onBoardingList[indexPath.item]
        cell.imageView.image = UIImage(named: onBoarding.image)
        cell.titleLabel.text = onBoarding.title
        cell.subtitleLabel.text = onBoarding.Subtitle
        
        return cell
    }
    
    // MARK : - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width
        return CGSize(width: width, height: 500)
    }
    
    //MARK - UICollectionViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int (scrollView.contentOffset.x / scrollView.frame.width)
        pageControl.currentPage = page
    }
    //MARK - Action
    
    @IBAction func pageControlValueChanged(_ sender: Any) {
        let page = pageControl.currentPage
        collectionView.scrollToItem(at: IndexPath(item: page, section: 0), at: .centeredHorizontally, animated: true)
    }
    // MARK: Helpers
    
    func loadOnBoardingList(){
        onBoardingList = [
            onBoarding(image: "img_onboarding1", title: "Gain total control of your money", Subtitle: "Become your own money manager and make every cent count"),
            onBoarding(image: "img_onboarding2", title: "Know where your money goes", Subtitle: "Track your transaction easily, with categories and financial report"),
            onBoarding(image: "img_onboarding3", title: "Planning ahead", Subtitle: "Setup your budget for each category so you in control")
                ]
        pageControl.numberOfPages = onBoardingList.count
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  OnBoardingViewCell.swift
//  Latihan2
//
//  Created by MACBOOK PRO on 20/03/23.
//

import UIKit

class OnBoardingViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}

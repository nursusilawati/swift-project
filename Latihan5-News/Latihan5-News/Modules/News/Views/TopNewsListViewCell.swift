//
//  TopNewsViewCell.swift
//  Latihan5-News
//
//  Created by MACBOOK PRO on 27/03/23.
//

import UIKit

protocol TopNewsListViewCellDelegate: AnyObject {
    func topNewsListViewCellPageControlValueChange(_ cell: TopNewsListViewCell)
}

class TopNewsListViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtiltleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    weak var delegate: TopNewsListViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func pageControlValueChanged(_ sender: Any){
        delegate?.topNewsListViewCellPageControlValueChange(self)
    }
    
}

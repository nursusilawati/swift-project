//
//  NewsViewCell.swift
//  Latihan5-News
//
//  Created by MACBOOK PRO on 28/03/23.
//

import UIKit

protocol NewsViewCellDelegate: AnyObject {
    func newsViewCellBookmarkButtonTapped(_ cell: NewsViewCell)
}
class NewsViewCell: UITableViewCell {

    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var bookmarkButton: UIButton!
    
    var delegate: NewsViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func bookmarkButtonTapped(_ sender: Any) {
        delegate?.newsViewCellBookmarkButtonTapped(self)
    }
    
}

//
//  TopNewsViewCell.swift
//  Latihan5-News
//
//  Created by MACBOOK PRO on 27/03/23.
//

import UIKit

class TopNewsViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
}

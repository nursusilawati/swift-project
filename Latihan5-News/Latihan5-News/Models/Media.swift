//
//  Media.swift
//  Latihan5-News
//
//  Created by MACBOOK PRO on 28/03/23.
//

import Foundation

struct Media: Decodable {
    let metadata: [MediaMetadata]
    
    enum CodingKeys: String, CodingKey {
        case metadata = "media-metadata"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        metadata = try container.decodeIfPresent([MediaMetadata].self, forKey: .metadata) ?? []
    }
}

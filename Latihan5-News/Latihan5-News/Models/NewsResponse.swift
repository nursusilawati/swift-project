//
//  NewsResponse.swift
//  Latihan5-News
//
//  Created by MACBOOK PRO on 28/03/23.
//

import Foundation

struct NewsResponse: Decodable {
    // decodable merupakan protokol  yang membantu kita
    
    let status: String
    let copyright: String
    let numResults: Int
    let results: [News]
    
    enum CodingKeys: String, CodingKey {
        case status
        case copyright
        case numResults = "num_results"
        case results
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.status = try container.decodeIfPresent(String.self, forKey: .status) ?? ""
        self.copyright = try container.decodeIfPresent(String.self, forKey: .copyright) ?? ""
        self.numResults = try container.decodeIfPresent(Int.self, forKey: .numResults) ?? 0
        self.results = try container.decodeIfPresent([News].self, forKey: .results) ?? []
    }
}

import UIKit

let people = [
"Jack Ryan", "John Wick", "Jason Bournce", "James Bond", "Tom Clancys", "Sam Finsher", "Jack Ryan Jr", "James Greer","Kathleen Ryan", "Scott Mitchell", "Sam Driscoll"
]

// Contoh closures  dengan parameter  dan return type
let theRyans = people.filter({(name: String)-> Bool in
    return name.contains("Ryan")
})


//contoh closures dimann reurn type nya bisa diomit (dihilangkan)
let theJack = people.filter{name in
    return name.contains("Ryan")
}

//Shorthand closures, $0, s1 sebagai pengganti argumen
let theJackPart2 = people.filter({$0.contains("Jack")})
theJackPart2

let theJackPart2 = people.filter{$0.contains("Jack")}
theJackPart2

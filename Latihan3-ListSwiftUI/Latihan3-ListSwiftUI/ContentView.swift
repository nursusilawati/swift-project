//
//  ContentView.swift
//  Latihan3-ListSwiftUI
//
//  Created by MACBOOK PRO on 24/03/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack(alignment: .center, spacing: 8) {
            Image(systemName: "brain.head.profile").font(.system(size: 60))
                .foregroundColor(.blue)
                .padding(.bottom, 24)
            
            Text("Hello iOS Developer")
                .font(.title)
                .fontWeight(.bold)
                .foregroundColor(.red)
                .padding(.bottom, 4)
            
            Text("Ini halaman dua")
                .font(.headline)
                .foregroundColor(.green)
            
            Text("Your time is limited, so don't waste it living someone else's life. Don't be trapped by dogma-which is living with the resulst of others people thinking")
                .multilineTextAlignment(.center)
                .lineLimit(3)
                .lineSpacing(10)
                .truncationMode(.tail)
                .padding()
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

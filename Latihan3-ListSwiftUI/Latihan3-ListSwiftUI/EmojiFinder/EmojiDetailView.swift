//
//  EmojiDetailView.swift
//  Latihan3-ListSwiftUI
//
//  Created by MACBOOK PRO on 24/03/23.
//

import SwiftUI

struct EmojiDetailView: View {
    
    let emojiDetail: EmojiModel
    
    var body: some View {
        VStack {
            Text(emojiDetail.emoji)
                .font(.system(size: 120))
                .padding(.bottom)
            
            Text(emojiDetail.name)
                .font(.system(.largeTitle, design: .rounded))
                .padding(.bottom)
            
            Text(emojiDetail.description)
                .font(.system(size: 18, design: .rounded))
                .lineSpacing(4)
                .multilineTextAlignment(.center)
            
            Spacer()
        }// VSTACK
        .padding()
    }
}


struct EmojiDetailView_Previews: PreviewProvider {
    static var previews: some View {
        EmojiDetailView(emojiDetail: EmojiModel(emoji: "😌", name: "Lemes", description: "hari ini terasa lebih lama, jam nya gamau jalan, stalk aja disana. Kenapa yaaaaaaaaaaaaa?????"))
    }
}

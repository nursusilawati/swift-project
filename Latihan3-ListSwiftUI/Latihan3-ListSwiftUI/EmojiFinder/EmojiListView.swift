//
//  EmojiListView.swift
//  Latihan3-ListSwiftUI
//
//  Created by MACBOOK PRO on 24/03/23.
//

import SwiftUI

struct EmojiListView: View {
    
    private var emojiSearchResults: [EmojiModel] {
        
        let results = EmojiProvider.all()
        if searchText.isEmpty{
            return results
        }else {
//            return results.filter {
//                index in index.name.lowercased().contains(searchText.lowercased())
//            }
            return results.filter{
                $0.name.lowercased()
                    .contains(searchText.lowercased())
            }
        }
        
    }
    
    @State private var searchText: String = ""
    
    private var suggestedResult: [EmojiModel] {
        if searchText.isEmpty {
            return []
        } else {
            return emojiSearchResults
        }
    }
    
    var body: some View {
        NavigationStack{
            List(emojiSearchResults) { result in
                NavigationLink(destination: {
                    EmojiDetailView(emojiDetail: result)
                }) {
                    Text("\(result.emoji)  \(result.name)")
                        .font(.title)
                    .padding(6)
                }
            }
            .listStyle(.plain)
            .navigationTitle("Emoji Data")
            .searchable(text: $searchText, placement: .navigationBarDrawer(displayMode: .always), prompt: "What emoji's that you are looking for?"
            ){
                ForEach(suggestedResult) { result in
                    Text("Hey, are you looking for \(result.name)?")
                        .searchCompletion(result.name)
                }
            }
        }
    }
}

struct EmojiListView_Previews: PreviewProvider {
    static var previews: some View {
        EmojiListView()
    
    }
}

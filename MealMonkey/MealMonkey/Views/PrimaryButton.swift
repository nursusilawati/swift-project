//
//  PrimaryButton.swift
//  MealMonkey
//
//  Created by MACBOOK PRO on 30/03/23.
//

import UIKit

@IBDesignable
class PrimaryButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 28 {
        didSet { update() }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    convenience init() {
        self.init(type: .system)
        setup()
    }
    
    func setup() {
        backgroundColor = UIColor(named: "Main", in: Bundle(for: PrimaryButton.self), compatibleWith: nil)
        titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        tintColor = .white
        
        update()
    }
    
    func update() {
        layer.cornerRadius = self.cornerRadius
        layer.masksToBounds = true
    }

}

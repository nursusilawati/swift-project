//
//  SecondaryButton.swift
//  MealMonkey
//
//  Created by MACBOOK PRO on 30/03/23.
//

import UIKit

class SecondaryButton: PrimaryButton {
    
    override func setup() {
        super.setup()
        
        backgroundColor = .clear
        tintColor = UIColor(named: "Main", in: Bundle(for: SecondaryButton.self), compatibleWith: nil)
        
        layer.borderWidth = 1
        layer.borderColor = tintColor.cgColor
    }
    
}

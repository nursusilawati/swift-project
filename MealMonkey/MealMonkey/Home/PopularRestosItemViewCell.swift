//
//  PopularRestosItemViewCell.swift
//  MealMonkey
//
//  Created by MACBOOK PRO on 10/04/23.
//

import UIKit

class PopularRestosItemViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var foodCategoryLabel: UILabel!
    @IBOutlet weak var restoCategoryLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
}

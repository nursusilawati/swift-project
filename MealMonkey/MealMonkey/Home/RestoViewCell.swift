//
//  RestoViewCell.swift
//  MealMonkey
//
//  Created by MACBOOK PRO on 04/04/23.
//

import UIKit

class RestoViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var restoCategorylabel: UILabel!
    @IBOutlet weak var foodcategoryLabel: UILabel!
    
}

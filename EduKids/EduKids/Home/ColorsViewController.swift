//
//  ColorsViewController.swift
//  EduKids
//
//  Created by MACBOOK PRO on 12/04/23.
//

import UIKit
import AVFoundation
import MediaPlayer

private let reuseIdentifier = "Cell"

class ColorsViewController: UIViewController {

    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var colors : [Colors] = []
    var audioPlayer: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadColors()
        setup()
        // Do any additional setup after loading the view.
    }
    func setup(){
        collectionView.dataSource = self
        //        collectionView.delegate = self
        collectionView.backgroundView = bgImageView
    }
    func playAudio(name:String){
        
        guard let url = Bundle.main.url(forResource:name, withExtension: "wav") else { return }
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer?.play()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func loadColors() {
        colors = [
            Colors(img: "red", name: "0", audio: ""),
            Colors(img: "yellow", name: "1", audio: ""),
            Colors(img: "black", name: "2", audio: ""),
            Colors(img: "white", name: "3", audio: ""),
            Colors(img: "green", name: "4", audio: ""),
            Colors(img: "purple", name: "5", audio: ""),
            Colors(img: "brown", name: "6", audio: ""),
            Colors(img: "blue", name: "7", audio: ""),
            Colors(img: "orange", name: "8", audio: ""),
            Colors(img: "pink", name: "9", audio: "")
        ]
    }
}

extension ColorsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colors_view_cell", for: indexPath) as! ColorsViewCell
        let colors = colors[indexPath.item]
        
        cell.imageView.image = UIImage(named: colors.img)
        cell.delegate = self
        return cell
    }
}

extension ColorsViewController: SoundDelegate{
    func soundPlay(_ cell: ColorsViewCell) {
        if let indexPath = collectionView.indexPath(for: cell) {
            let audio = colors[indexPath.item]
            
            playAudio(name: audio.audio)
        }
    }
}

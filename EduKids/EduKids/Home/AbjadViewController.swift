//
//  AbjadViewController.swift
//  EduKids
//
//  Created by MACBOOK PRO on 11/04/23.
//

import UIKit
import AVFoundation

//AVSpeechSynthesizerDelegate

class AbjadViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bgImageView: UIImageView!
    
    var abjad : [Abjad] = []
    
    var audioPlayer: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAbjad()
        setup()
        // Do any additional setup after loading the view.
    }
    func setup(){
        collectionView.dataSource = self
//        collectionView.delegate = self
        
        collectionView.backgroundView = bgImageView
    }
    func playAudio(name:String){
           
            guard let url = Bundle.main.url(forResource:name, withExtension: "wav") else { return }
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer?.play()
            } catch {
                print(error.localizedDescription)
            }
        }
    
    func loadAbjad() {
        abjad = [
                Abjad(img: "a", name: "a", audio: "A"),
                Abjad(img: "b", name: "b", audio: "B"),
                Abjad(img: "c", name: "c", audio: "C"),
                Abjad(img: "d", name: "d", audio: "D"),
                Abjad(img: "e", name: "e", audio: "E"),
                Abjad(img: "f", name: "f", audio: "F"),
                Abjad(img: "g", name: "g", audio: "G"),
                Abjad(img: "h", name: "h", audio: "H"),
                Abjad(img: "i", name: "i", audio: "I"),
                Abjad(img: "j", name: "j", audio: "J"),
                Abjad(img: "k", name: "k", audio: "K"),
                Abjad(img: "l", name: "l", audio: "L"),
                Abjad(img: "m", name: "m", audio: "M"),
                Abjad(img: "n", name: "n", audio: "N"),
                Abjad(img: "o", name: "o", audio: "O"),
                Abjad(img: "p", name: "p", audio: "P"),
                Abjad(img: "q", name: "q", audio: "Q"),
                Abjad(img: "r", name: "r", audio: "R"),
                Abjad(img: "s", name: "s", audio: "S"),
                Abjad(img: "t", name: "t", audio: "T"),
                Abjad(img: "u", name: "u", audio: "U"),
                Abjad(img: "v", name: "v", audio: "V"),
                Abjad(img: "w", name: "w", audio: "W"),
                Abjad(img: "x", name: "x", audio: "X"),
                Abjad(img: "y", name: "y", audio: "Y"),
                Abjad(img: "z", name: "z", audio: "Z")
                ]
    }
}

extension AbjadViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return abjad.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "abjad_view_cell", for: indexPath) as! AbjadViewCell
        let abjad = abjad[indexPath.item]
        
        cell.imageView.image = UIImage(named: abjad.img)
        cell.delegate = self
        return cell
    }
}

extension AbjadViewController: soundButtonDelegate{
    func soundPlay(_ cell: AbjadViewCell) {
        if let indexPath = collectionView.indexPath(for: cell) {
            let audio = abjad[indexPath.item]
            
            playAudio(name: audio.audio)
        }
    }
}
    

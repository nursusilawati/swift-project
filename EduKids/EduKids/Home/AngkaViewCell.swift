//
//  AngkaViewCell.swift
//  EduKids
//
//  Created by MACBOOK PRO on 11/04/23.
//

import UIKit
protocol SoundButtonDelegate: AnyObject {
    func soundPlay(_ cell:AngkaViewCell)
}

class AngkaViewCell: UICollectionViewCell {
    
    weak var delegate : SoundButtonDelegate?
    @IBAction func buttonAngka(_ sender: Any) {
        delegate?.soundPlay(self)
    }
    @IBOutlet weak var imageView: UIImageView!
}

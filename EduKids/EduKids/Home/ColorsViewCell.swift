//
//  ColorsViewCell.swift
//  EduKids
//
//  Created by MACBOOK PRO on 12/04/23.
//

import UIKit
protocol SoundDelegate: AnyObject {
    func soundPlay(_ cell:ColorsViewCell)
}

class ColorsViewCell: UICollectionViewCell {
    weak var delegate : SoundDelegate?
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var buttonColors: UIButton!
}

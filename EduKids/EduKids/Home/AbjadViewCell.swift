//
//  AbjadViewCell.swift
//  EduKids
//
//  Created by MACBOOK PRO on 10/04/23.
//

import UIKit

protocol soundButtonDelegate: AnyObject {
    func soundPlay(_ cell:AbjadViewCell)
}


class AbjadViewCell: UICollectionViewCell {

    weak var delegate : soundButtonDelegate?
    
    @IBAction func buttonAbjad(_ sender: Any) {
        delegate?.soundPlay(self)
    }
    @IBOutlet weak var imageView: UIImageView!
    
    
}

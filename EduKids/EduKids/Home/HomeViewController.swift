//
//  HomeViewController.swift
//  EduKids
//
//  Created by MACBOOK PRO on 09/04/23.
//

import UIKit
import AVFoundation

class HomeViewController: UIViewController {
    
    @IBOutlet weak var ImageViewJudul: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    
    var audioPlayer: AVAudioPlayer?
    
    override func viewDidLoad() {
            super.viewDidLoad()
        }
    
    override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)

            // Memulai animasi
            moveContent()
            playAudio()
        }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Berhenti audio ketika user meninggalkan halaman
        audioPlayer?.stop()
    }
    func playAudio() {
            guard let path = Bundle.main.path(forResource: "backsound", ofType: "mp3") else { return }
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
                audioPlayer?.numberOfLoops = -1 // Untuk memutar audio terus menerus
                audioPlayer?.play()
            } catch {
                print("Terjadi kesalahan saat memutar audio")
            }
        }
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "abjad_view_cell" {
                // Berhenti audio ketika user berpindah ke halaman selanjutnya
                audioPlayer?.stop()
            }
        }

        func moveContent() {
            let imageViewOrigin = self.imageView.center
            let ImageViewJudulOrigin = self.ImageViewJudul.center
//            let imageViewAbjadOrigin = self.imageViewAbjad.center
            UIView.animate(withDuration: 1.0, delay: 0.5, options: [.repeat, .autoreverse], animations: {
                // Menggerakkan imageView ke posisi yang ditentukan
                self.imageView.center = CGPoint(x: 180, y: 0)
                self.ImageViewJudul.center = CGPoint(x: 200, y: 0)
//                self.imageViewAbjad.center = CGPoint(x: 100, y: 0)

                // Menggerakkan label ke posisi yang ditentukan
            }, completion: { _ in
                self.imageView.center = imageViewOrigin
                self.ImageViewJudul.center = ImageViewJudulOrigin
//                self.imageViewAbjad.center = imageViewAbjadOrigin
            })
        }
    }
    
//    let imageView = UIImageView(image: UIImage(named: "AREA BELAJAR"))
//        let screenWidth = UIScreen.main.bounds.width
//        let screenHeight = UIScreen.main.bounds.height

//    override func viewDidLoad() {
//        super.viewDidLoad()
//        imageView.frame = CGRect(x: 0, y: 0, width: 50, height: 50) // Atur ukuran gambar Anda
//                view.addSubview(imageView)
//                moveImage()

        // Do any additional setup after loading the view.
//    }
//    func moveImage() {
//            UIView.animate(withDuration: 1.0, delay: 0.25, options: [.repeat, .autoreverse], animations: {
//                self.imageView.center = CGPoint(x: 0, y: 0) // Atur batas koordinat untuk posisi gambar Anda
//            }, completion: nil)
//        }
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */



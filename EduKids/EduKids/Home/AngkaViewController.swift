//
//  CollectionViewController.swift
//  EduKids
//
//  Created by MACBOOK PRO on 11/04/23.
//

import UIKit
import AVFoundation
import MediaPlayer


class AngkaViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bgImageView: UIImageView!
    var angka : [Angka] = []
    var audioPlayer: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAngka()
        setup()
        // Do any additional setup after loading the view.
    }
    func setup(){
        collectionView.dataSource = self
        //        collectionView.delegate = self
        collectionView.backgroundView = bgImageView
    }
    func playAudio(name:String){
        
        guard let url = Bundle.main.url(forResource:name, withExtension: "wav") else { return }
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer?.play()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func loadAngka() {
        angka = [
            Angka(img: "0", name: "0", audio: "0"),
            Angka(img: "1", name: "1", audio: "1"),
            Angka(img: "2", name: "2", audio: "2"),
            Angka(img: "3", name: "3", audio: "3"),
            Angka(img: "4", name: "4", audio: "4"),
            Angka(img: "5", name: "5", audio: "5"),
            Angka(img: "6", name: "6", audio: "6"),
            Angka(img: "7", name: "7", audio: "7"),
            Angka(img: "8", name: "8", audio: "8"),
            Angka(img: "9", name: "9", audio: "9"),
            Angka(img: "10", name: "10", audio: "10")
        ]
    }
}

extension AngkaViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return angka.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "angka_view_cell", for: indexPath) as! AngkaViewCell
        let angka = angka[indexPath.item]
        
        cell.imageView.image = UIImage(named: angka.img)
        cell.delegate = self
        return cell
    }
}

extension AngkaViewController: SoundButtonDelegate{
    func soundPlay(_ cell: AngkaViewCell) {
        if let indexPath = collectionView.indexPath(for: cell) {
            let audio = angka[indexPath.item]
            
            playAudio(name: audio.audio)
        }
    }
}

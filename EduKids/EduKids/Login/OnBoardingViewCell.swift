//
//  onBoardingViewCell.swift
//  EduKids
//
//  Created by MACBOOK PRO on 08/04/23.
//

import UIKit

class OnBoardingViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    
}

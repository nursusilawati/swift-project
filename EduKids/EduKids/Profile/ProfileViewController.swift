//
//  ProfileViewController.swift
//  EduKids
//
//  Created by MACBOOK PRO on 10/04/23.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    var profileImage : UIImage? {
        didSet {
            profileImageView.image = profileImage
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let user = UserDefaults.standard.user

        nameTextField.text = user?.name
        emailTextField.text = user?.email
    }
    

    @IBAction func cameraButtonTapped(_ sender: Any) {
        pickImage()
    }
    
    @IBAction func signOutButtonTapped(_ sender: Any) {
        UserDefaults.standard.deleteToken()
        UserDefaults.standard.deleteUser()
        goToAuth()
    }
    
    
    func pickImage() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title:"Camera", style: .default, handler: { _ in
            self.showImagePicker(source: .camera)
        }))
        
        actionSheet.addAction(UIAlertAction(title:"Photo Library", style: .default, handler: { _ in
            self.showImagePicker(source: .photoLibrary)
        }))
        
        actionSheet.addAction(UIAlertAction(title:"Cancel", style: .cancel))
        present(actionSheet, animated: true)
    }
    
    func showImagePicker(source: UIImagePickerController.SourceType) {
        let viewController = UIImagePickerController()
        viewController.sourceType = source
        viewController.delegate = self
        
        present(viewController, animated: true)
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[.originalImage] as? UIImage
        profileImage = image
        dismiss(animated: true)
    }
}

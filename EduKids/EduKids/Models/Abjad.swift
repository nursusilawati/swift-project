//
//  Abjad.swift
//  EduKids
//
//  Created by MACBOOK PRO on 11/04/23.
//

import Foundation


struct Abjad{
    let img: String
    let name: String
    let audio: String
}

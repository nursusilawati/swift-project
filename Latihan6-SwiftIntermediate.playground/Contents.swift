import UIKit

//If Statement
let temperature = 24

if temperature <= 24 {
    print("====> Dingin sekali....")
}

// If-else statement
let temperatureAc = 30

if temperatureAc >= 30 {
    print("====> Udara Panas")
}else {
    print("====> udara dingin")
}

// if-else boolean value
let temperatureRuangan = 45

if temperatureRuangan >= 35 && temperatureRuangan <= 40 {
    print("====> udara palembang banget deh")
} else if (temperatureRuangan != 22) {
    print("====> Bandung banget")
} else {
    print("====> warga surabaya udah biasa")
}

//switch statement
let krlDeparture = 6

switch krlDeparture {
case 5:
    print("KRL masih kosong")
case 6:
    print("KRL mulai chaos")
case 7:
    print("Jadi Cakwe Goyang")
case 8:
    print("Udah siap-siap Resign")
default:
    print("Anda udah layak jadi pengangguran")
}


// Ternary Operator
var flashSale: Int
let a = 2
let b = 4

if a > b {
    flashSale = a
} else {
    flashSale = b
}


// rewrite
flashSale = a > b ? a : b


// String Interpolation
let name = "John Wick"
let age = 50

print("====> \(name) umurnya \(age)")

//Function
func displayPi() {
    print("3.1415")
}

displayPi()

//Function dengan parameter
func triple(value: Int) {
    let result = value * 3
    print("====> If you multiply \(value) by 3, you'll get \(result)")
}

// Function dengan multiple parameter
func multiply(firstNumber: Int, secondNumber: Int) {
    let result = firstNumber * secondNumber
    print("==> The result is \(result)")
}

let hasil: () = multiply(firstNumber: 10, secondNumber: 5)
print(hasil)

//function ada return value
func multiply2(fourthNumber: Int, fifthNumber: Int) -> Int {
    let result = fourthNumber + fifthNumber
    print("===> the result if \(result)")
    
    return result
}

let myResult = multiply2(fourthNumber: 10, fifthNumber: 5)
print("===> 10 tambah 5 sama dengan \(myResult)")


//Function yang ada argumen labels
func sayHello(to person: String, and anotherPerson: String) {
    print("Hello \(person) and \(anotherPerson)")
}

sayHello(to: "John", and: "Lisa")

//omitting labels
func add(_ firstNumber: Int, to secondNumber: Int) -> Int {
    return firstNumber + secondNumber
}

add(20, to: 11)

//Struct
struct Shirt {
    var size: String
    var color: String
}

let myShirt = Shirt(size: "L", color: "Pink")
let yourShirt = Shirt(size: "XL", color: "Blue")

// dari API biasanya dapat data JSON berupa Dictionary
var myDict: [String: Int] = [
    "a": 1,
    "b": 2,
    "c": 3
]

//menghapus item dari data dictionary
myDict.removeValue(forKey: "c")

//mengakses item dari dictionary
let item = myDict["b"]

//mengupdate item dari dictionary
myDict["a"] = 5

//looping item
for (key, value) in myDict {
    print("====> isi dictionary saya adalah \(key): \(value)")
}

//mendapatkan semua values
let values = Array(myDict.keys)
print(values)
